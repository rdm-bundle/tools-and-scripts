# Create an Application Profile in Coscine using and Excel Spreadsheet

This repository contains an [excel file](https://git.rwth-aachen.de/rdm-bundle/tools-and-scripts/-/blob/main/speadsheetToAp/MD_table.xlsx) which collects all desired information to be included in a custom application profile, as well as a [Jupyter Notebook](https://git.rwth-aachen.de/rdm-bundle/tools-and-scripts/-/blob/main/speadsheetToAp/AP_from_excel.ipynb) that creates the required turtle files for the application profile. More information and instructions can be found in both files. The [AIMS Frontend Profile Generator](https://coscine.rwth-aachen.de/coscine/apps/aimsfrontend/#/) should be used to check that the profile compiles and renders correctly, to add additional field properties, and to submit the profile.

*Please note: updates to the Terms section of the Jupyter Notebook are still pending.*
